package demo.payment.bsd.paymentapp.processors;

import demo.payment.bsd.paymentapp.model.CardReadResponse;
import demo.payment.bsd.paymentapp.model.CardReadTags;
import demo.payment.bsd.paymentapp.model.CardResponse;
import demo.payment.bsd.paymentapp.model.DukptInitializeResponse;
import demo.payment.bsd.paymentapp.model.RandomKeyAnswer;
import demo.payment.bsd.paymentapp.utils.Constants;
import demo.payment.bsd.paymentapp.utils.Utils;

public class MessageProcessor {

    public static Constants.AnswerType getMessageType(String msg) {
        if (msg.equals("")) {
            return Constants.AnswerType.NONE;
        }

        String messageIdentifier = msg.substring(0, 4);

        if (messageIdentifier.indexOf("3") == 0) {
            String transactionId = msg.substring(12, 14);
            switch (messageIdentifier) {
                case "3000" :
                    if (transactionId.equals("13")) {
                        return Constants.AnswerType.REQUEST_DUKPT_INSTALLATION;
                    }
                    break;
                case "3010" :
                    if (transactionId.equals("12")) {
                        return Constants.AnswerType.ANSWER_RANDOM_KEY_REQUEST;
                    }
                    if (transactionId.equals("13")) {
                        return Constants.AnswerType.ANSWER_DUKPT_INSTALLATION;
                    }
                    if (transactionId.equals("11")) {
                        return Constants.AnswerType.REQUEST_CONFIGURATION;
                    }
                    break;
                default :
                    return Constants.AnswerType.NONE;
            }
        }

        if (messageIdentifier.equals("0100")) {
            return Constants.AnswerType.REQUEST_TRANSACTION_EMV;
        }

        if (messageIdentifier.equals("0010")) {
            return Constants.AnswerType.REQUEST_TRANSACTION_MAGSTRIPE;
        }

        if (messageIdentifier.equals("0101")) {
            return Constants.AnswerType.REQUEST_101;
        }

        return Constants.AnswerType.NONE;
    }

    private static int nextStringIndex = 0;
    private static String message;
    public static <E> E processMessage(Constants.AnswerType messageType, String msg) {
        nextStringIndex = 0;
        message = msg;

        switch (messageType) {
            case ANSWER_RANDOM_KEY_REQUEST:
                RandomKeyAnswer randomKeyAnswer = new RandomKeyAnswer();
                randomKeyAnswer.setMessageIdentifier(getNextString(4));
                randomKeyAnswer.setMessageLength(getNextString(4));
                randomKeyAnswer.setStatus(getNextString(4));
                randomKeyAnswer.setTransacitonId(getNextString(2));
                randomKeyAnswer.setSerialNumber(getNextString(11));
                randomKeyAnswer.setEncryptedBlockLength(getNextString(4));
                randomKeyAnswer.setEncryptedBlock(getNextString(Integer.valueOf(randomKeyAnswer.getEncryptedBlockLength()) * 2));
                randomKeyAnswer.setRandomKeyKcv(getNextString(6));
                randomKeyAnswer.setEncryptedBlockCrc32(getNextString(8));

                return (E)randomKeyAnswer;

            case ANSWER_DUKPT_INSTALLATION:
                DukptInitializeResponse dukptInitializeResponse = new DukptInitializeResponse();
                dukptInitializeResponse.setMessageIdentifier(getNextString(4));
                dukptInitializeResponse.setMessageLength(getNextString(4));
                dukptInitializeResponse.setStatus(getNextString(4));
                dukptInitializeResponse.setTransactionId(getNextString(2));
                dukptInitializeResponse.setSerialNumber(getNextString(2));

                return (E)dukptInitializeResponse;

            case REQUEST_TRANSACTION_EMV:
                CardReadResponse cardEmv =  new CardReadResponse();
                cardEmv.setMsgId(getNextString(4));
                cardEmv.setMsgLength(getNextString(4));
                cardEmv.setP0(getNextString(4));
                cardEmv.setP1(getNextString(11));
                cardEmv.setP2(getNextString(20));
                cardEmv.setP3(getNextString(6));
                cardEmv.setP4(getNextString(10));
                cardEmv.setP5(getNextString(2));
                cardEmv.setP6(getNextString(1));
                cardEmv.setP7(getNextString(2));
                cardEmv.setP8(getNextString(Integer.valueOf(cardEmv.getP7())));
                cardEmv.setP9(getNextString(2));
                cardEmv.setP10(getNextString(6));
                cardEmv.setP11(getNextString(2));
                cardEmv.setP12(getNextString(4));
                cardEmv.setP13(getNextString(4));
                cardEmv.setP14(getNextString(4));
                cardEmv.setP15(getNextString(Integer.valueOf(cardEmv.getP14())));
                cardEmv.setP16(getNextString(8));
                cardEmv.setP17(getNextString(8));
                cardEmv.setP18(getNextString(4));
                cardEmv.setP19(getNextString(Integer.valueOf(cardEmv.getP18())));
                cardEmv.setP20(getNextString(30));


                nextStringIndex = 0;
                CardReadTags tags = new CardReadTags();

                for(String tag: Constants.ARRAY_TAG){
                    tags.setTag(tag, getTag(tag, cardEmv.getP19()));
                }

                CardResponse cardResponseEmv = new CardResponse();
                cardResponseEmv.setCardReadResponse(cardEmv);
                cardResponseEmv.setCardReadTags(tags);

                return (E)cardResponseEmv;

            case REQUEST_TRANSACTION_MAGSTRIPE:
                CardReadResponse magstripe = new CardReadResponse();
                magstripe.setMsgId(getNextString(4));
                magstripe.setMsgLength(getNextString(4));
                magstripe.setP0(getNextString(4));
                magstripe.setP1(getNextString(11));
                magstripe.setP2(getNextString(20));
                magstripe.setP3(getNextString(6));
                magstripe.setP4(getNextString(10));
                magstripe.setP5(getNextString(2));
                magstripe.setP6(getNextString(2));
                magstripe.setP7(getNextString(Integer.valueOf(magstripe.getP6())));
                magstripe.setP8(getNextString(2));
                if(Integer.valueOf(magstripe.getP8()) > 0){
                    magstripe.setP9(getNextString(Integer.valueOf(magstripe.getP8())));
                }
                magstripe.setP10(getNextString(1));
                magstripe.setP11(getNextString(1));
                magstripe.setP12(getNextString(2));
                magstripe.setP13(getNextString(6));
                magstripe.setP14(getNextString(2));
                magstripe.setP15(getNextString(4));
                magstripe.setP16(getNextString(4));
                magstripe.setP17(getNextString(4));
                magstripe.setP18(getNextString((Integer.valueOf(magstripe.getP17()))));
                magstripe.setP19(getNextString(8));
                magstripe.setP20(getNextString(8));

                CardResponse cardResponseMagstripe = new CardResponse();
                cardResponseMagstripe.setCardReadResponse(magstripe);

                return (E)cardResponseMagstripe;

            case REQUEST_CONFIGURATION:
                CardReadResponse configurationResponse = new CardReadResponse();
                configurationResponse.setMsgId(getNextString(4));
                configurationResponse.setMsgLength(getNextString(4));
                configurationResponse.setP0(getNextString(4));
                configurationResponse.setP1(getNextString(2));
                configurationResponse.setP2(getNextString(11));
                configurationResponse.setP3(getNextString(8));
                configurationResponse.setP4(getNextString(3));
                configurationResponse.setP5(getNextString(3));
                configurationResponse.setP6(getNextString(3));
                configurationResponse.setP7(getNextString(2));
                configurationResponse.setP8(getNextString(2));
                configurationResponse.setP9(getNextString(8));
                configurationResponse.setP10(getNextString(4));

                return (E)configurationResponse;
            case REQUEST_101:
                CardReadResponse cardResponse = new CardReadResponse();

                cardResponse.setMsgId(getNextString(4));
                cardResponse.setMsgLength(getNextString(4));
                cardResponse.setP0(getNextString(4));
                cardResponse.setP1(getNextString(11));

                return (E)cardResponse;
        }

        return null;
    }

    private static String getNextString (int length) {
        String res = message.substring(nextStringIndex, nextStringIndex + length);
        nextStringIndex += length;
        return res;
    }

    private static String getTag(String tag, String cad){
        String result="";

        try{
            String sTag = cad.substring(nextStringIndex, nextStringIndex + tag.length());
            if(sTag.equals(tag)){
                nextStringIndex += tag.length();
                int tag_len = Integer.parseInt(cad.substring(nextStringIndex, nextStringIndex + 2), 16) * 2;
                nextStringIndex += 2;
                String tag_value = cad.substring(nextStringIndex, nextStringIndex + tag_len);
                result = tag_value;
                nextStringIndex += tag_len;
            }
        }catch (Exception ex){
            Utils.logE(ex);
        }

        return result;
    }
}
