package demo.payment.bsd.paymentapp.tasks;

import android.os.AsyncTask;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import demo.payment.bsd.paymentapp.model.CardReadResponse;
import demo.payment.bsd.paymentapp.model.CardReadTags;
import demo.payment.bsd.paymentapp.model.CardResponse;
import demo.payment.bsd.paymentapp.model.TokenQ6;
import demo.payment.bsd.paymentapp.tasks.actions.IStompEvents;
import demo.payment.bsd.paymentapp.tasks.actions.RestRequester;
import demo.payment.bsd.paymentapp.tasks.actions.StompConnector;
import demo.payment.bsd.paymentapp.utils.Utils;

public class SaleTask extends AsyncTask<Object, Void, String> {
    private String result;

    private IStompEvents delegate = null;
    public SaleTask(IStompEvents delegate) {
        this.delegate = delegate;
    }

    public SaleTask() {}

    @Override
    protected String doInBackground(Object... objects) {
        try {
            CardResponse card = (CardResponse) objects[0];
            CardReadResponse rc = card.getCardReadResponse();
            CardReadTags mt = card.getCardReadTags() == null ? new CardReadTags() : card.getCardReadTags();
            TokenQ6 tokenQ6 = card.getTokenQ6();

            Boolean chipSale = rc.getMsgId().equals("0100");

            JSONObject tokenC4 = new JSONObject();
            if (rc.getMsgId().equals("0100")) {
                tokenC4.put("cardHolderIdentityMode", (rc.getP5().equals("2") ? "2" : "1"));
            } else {
                tokenC4.put("cardHolderIdentityMode", "1");
            }

            JSONObject tokenB2 = new JSONObject();
            tokenB2.put("criptoInfoData",           mt.getTagValue("9F27"));
            tokenB2.put("tvr",                      mt.getTagValue("95"));
            tokenB2.put("arqc",                     mt.getTagValue("9F26"));
            tokenB2.put("aip",                      mt.getTagValue("82"));
            tokenB2.put("atc",                      mt.getTagValue("9F36"));
            tokenB2.put("transactionDate",          mt.getTagValue("9A"));
            tokenB2.put("transactionType",          mt.getTagValue("9C"));
            tokenB2.put("unpredictableNumber",      mt.getTagValue("9F37"));
            tokenB2.put("issuerData",               mt.getTagValue("9F10"));

            JSONObject tokenB3 = new JSONObject();
            tokenB3.put("termCapabilities",         mt.getTagValue("9F33"));
            tokenB3.put("termType",                 mt.getTagValue("9F35"));
            tokenB3.put("appVerNum",                mt.getTagValue("9F09"));
            tokenB3.put("cvmRSLTS",                 mt.getTagValue("9F34"));
            tokenB3.put("dfName",                   mt.getTagValue("84"));

            JSONObject tokenB4 = new JSONObject();
            tokenB4.put("appPanSeqNum",             mt.getTagValue("5F34"));

            JSONObject tokenEZ = new JSONObject();
            tokenEZ.put("keySerialNumber",          rc.getP2());
            tokenEZ.put("cipherCounter",            rc.getP3());
            tokenEZ.put("cipherFailCounter",        rc.getP5());
            tokenEZ.put("cardReadMode",             rc.getCardReadMode());

            if (chipSale) {
                tokenEZ.put("cvv2Length",           rc.getP11());
                tokenEZ.put("track2Length",         rc.getP13());
                tokenEZ.put("track1Length",         rc.getP12());
                tokenEZ.put("cipherDataTrack2Cvv2", rc.getP15());
                tokenEZ.put("panLast4Digits",       rc.getP8());
                tokenEZ.put("cipherDataCRC32",      rc.getP16());
            } else {
                tokenEZ.put("cvv2Length",           rc.getP14());
                tokenEZ.put("track2Length",         rc.getP16());
                tokenEZ.put("track1Length",         rc.getP5());
                tokenEZ.put("cipherDataTrack2Cvv2", rc.getP18());
                tokenEZ.put("panLast4Digits",       rc.getP7());
                tokenEZ.put("cipherDataCRC32",      rc.getP19());
            }

            JSONObject tokenEY = new JSONObject();
            tokenEY.put("track1Length",             rc.getP12());
            tokenEY.put("track1Encript",            rc.getP15());
            tokenEY.put("track1CRC32",              rc.getP17());

            JSONObject tokenES = new JSONObject();
            tokenES.put("needNewKey",               "false");
            tokenES.put("pinpadSerialNumber",       rc.getP1());

            JSONObject tokenq6 = new JSONObject();
            if (tokenQ6 != null) {
                tokenq6.put("diferimiento", tokenQ6.getDiferimiento());
                tokenq6.put("numeroPagos", tokenQ6.getNumeroPagos());
                tokenq6.put("promocion", tokenQ6.getPromocion());
            }

            JSONObject pinpad = new JSONObject();
            pinpad.put("serial",                    rc.getP1());
            pinpad.put("tokenB2",                   tokenB2);
            pinpad.put("tokenB3",                   tokenB3);
            pinpad.put("tokenB4",                   tokenB4);
            pinpad.put("tokenEZ",                   tokenEZ);
            pinpad.put("tokenES",                   tokenES);
            pinpad.put("tokenC4",                   tokenC4);
            pinpad.put("tokenQ6",                   tokenQ6 == null ? null : tokenq6);


            final JSONObject request = new JSONObject();
            request.put("pinpad",              pinpad);
            request.put("total",               String.format("%012d", Integer.valueOf(rc.getP4())));
            request.put("chipSale",            chipSale);

            new StompConnector(new IStompEvents() {
                @Override
                public void onComplete() {
                    String uuid = Utils.GetUuid(false);
                    RestRequester.SendRequest("sale/" + uuid + ".json", request);
                }

                @Override
                public void onError(Throwable ex) {
                    delegate.onError(ex);
                }

                @Override
                public void onNext(String stompMessage) {
                    delegate.onNext(stompMessage);
                }
            }).runStomp(Utils.GetUuid(true));
        } catch (Exception ex) {
            Utils.logE(ex);
        }
        return null;
    }
}
