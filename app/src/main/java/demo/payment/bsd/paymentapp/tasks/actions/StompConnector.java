package demo.payment.bsd.paymentapp.tasks.actions;

import org.java_websocket.WebSocket;

import java.util.ArrayList;
import java.util.List;

import demo.payment.bsd.paymentapp.utils.Constants;
import demo.payment.bsd.paymentapp.utils.Utils;
import rx.Notification;
import rx.Observer;
import rx.functions.Action0;
import rx.functions.Action1;
import ua.naiksoftware.stomp.LifecycleEvent;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompHeader;
import ua.naiksoftware.stomp.client.StompClient;
import ua.naiksoftware.stomp.client.StompMessage;

public class StompConnector {
    final IStompEvents callbacks;

    public StompConnector(IStompEvents callbacks) {
        this.callbacks = callbacks;
    }

    public void runStomp (String queue) {
        try {
            final StompClient mStompClient = Stomp.over(WebSocket.class, Constants.BASE_STOMP_URL);
            List<StompHeader> headers = new ArrayList<>();
            headers.add(new StompHeader("login", "queue"));
            headers.add(new StompHeader("passcode", "b6cnibnvA"));

            mStompClient.topic("/queue/" + queue)
            .subscribe(new Observer<StompMessage>() {
                @Override
                public void onCompleted() {
                    String bla = "";
                }

                @Override
                public void onError(Throwable e) {
                    Utils.StopProcessDialog();
                    callbacks.onError(e);
                }

                @Override
                public void onNext(StompMessage stompMessage) {
                    mStompClient.disconnect();
                    callbacks.onNext(stompMessage.getPayload());
                }
            });

            mStompClient.lifecycle().subscribe(new Action1<LifecycleEvent>() {
                @Override
                public void call(LifecycleEvent lifecycleEvent) {
                    if (lifecycleEvent.getType() == LifecycleEvent.Type.OPENED) {
                        callbacks.onComplete();
                    }
                }
            });

            mStompClient.connect(headers);
        } catch (Exception ex) {
            callbacks.onError(ex);
        }
    }
}
