package demo.payment.bsd.paymentapp.tasks.actions;

public interface IStompEvents {
    void onComplete ();
    void onError(Throwable ex);
    void onNext(String stompMessage);
}
