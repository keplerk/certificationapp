package demo.payment.bsd.paymentapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DukptResponse {
    @JsonProperty("CRC32")
    private String CRC32;
    @JsonProperty("Ksn")
    private String ksn;
    @JsonProperty("Bdk")
    private String bdk;
    @JsonProperty("CheckValue")
    private String checkValue;

    public String getCRC32() {
        return CRC32;
    }

    public void setCRC32(String CRC32) {
        this.CRC32 = CRC32;
    }

    public String getKsn() {
        return ksn;
    }

    public void setKsn(String ksn) {
        this.ksn = ksn;
    }

    public String getBdk() {
        return bdk;
    }

    public void setBdk(String bdk) {
        this.bdk = bdk;
    }

    public String getCheckValue() {
        return checkValue;
    }

    public void setCheckValue(String checkValue) {
        this.checkValue = checkValue;
    }
}
