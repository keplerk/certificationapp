package demo.payment.bsd.paymentapp.tasks;

import android.os.AsyncTask;

import org.json.JSONObject;

import demo.payment.bsd.paymentapp.model.RandomKeyAnswer;
import demo.payment.bsd.paymentapp.tasks.actions.IStompEvents;
import demo.payment.bsd.paymentapp.tasks.actions.RestRequester;
import demo.payment.bsd.paymentapp.tasks.actions.StompConnector;
import demo.payment.bsd.paymentapp.utils.Utils;

public class InitializeTask extends AsyncTask<Object, Void, String> {
    private String result;

    private IStompEvents delegate = null;
    public InitializeTask(IStompEvents delegate) {
        this.delegate = delegate;
    }

    public InitializeTask() {}

    @Override
    protected String doInBackground(Object... objects) {
        try {
            RandomKeyAnswer answer = (RandomKeyAnswer) objects[0];

            JSONObject tokenEw = new JSONObject();
            tokenEw.put("randomKey", answer.getEncryptedBlock());
            tokenEw.put("randomKeyValidation", answer.getRandomKeyKcv());
            tokenEw.put("randomKeyCRC32", answer.getEncryptedBlockCrc32());
            tokenEw.put("publicRSAKeyVersion", "N000ARFT22");
            tokenEw.put("paddingAlgorithm", "01");

            JSONObject tokenES = new JSONObject();
            tokenES.put("needNewKey", true);
            tokenES.put("pinpadSerialNumber", answer.getSerialNumber());

            final JSONObject request = new JSONObject();
            request.put("tokenEw", tokenEw);
            request.put("tokenEs", tokenES);

            new StompConnector(new IStompEvents() {
                @Override
                public void onComplete() {
                    String uuid = Utils.GetUuid(false);
                    RestRequester.SendRequest("auth/sync/" + uuid + ".json", request);
                }

                @Override
                public void onError(Throwable ex) {
                    delegate.onError(ex);
                }

                @Override
                public void onNext(String stompMessage) {
                    delegate.onNext(stompMessage);
                }
            }).runStomp(Utils.GetUuid(true));
        } catch (Exception ex) {
            Utils.logE(ex);
        }
        return null;
    }
}
