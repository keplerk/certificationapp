package demo.payment.bsd.paymentapp.utils;

import android.content.Intent;

import demo.payment.bsd.paymentapp.MainActivity;
import es.itos.pagomovil.service.PinpadService;

public class Pinpad {
    private static Pinpad mPinpad;
    public static Pinpad getInstance () {
        if (mPinpad == null) {
            mPinpad = new Pinpad();
        }

        return mPinpad;
    }

    public void startService() {
        PinpadService.start(Utils.activity.getApplicationContext());
    }

    public void stopService() {
        PinpadService.stop(Utils.activity.getApplicationContext());
    }

    private Constants.MessageType messsageType;

    public Constants.MessageType getMesssageType() {
        return messsageType;
    }

    public void setMesssageType(Constants.MessageType messsageType) {
        this.messsageType = messsageType;
    }

    public String currentMessage;

    public String getCurrentMessage() {
        return currentMessage;
    }

    public void setCurrentMessage(String currentMessage) {
        this.currentMessage = currentMessage;
    }

    public void sendRequest() {
        final Intent intent = new Intent(Utils.activity, PinpadService.class);
        intent.setAction(PinpadService.ACTION_OPERATION_REQUEST);
        intent.putExtra(PinpadService.EXTRA_MSG, currentMessage.getBytes());
        Utils.activity.startService(intent);

        this.setMesssageType(Constants.MessageType.NONE);
    }

    public void requestCancel () {
        final Intent intent = new Intent(Utils.activity, PinpadService.class);
        intent.setAction(PinpadService.ACTION_CANCEL_OPERATION_REQUEST);
        Utils.activity.startService(intent);
    }

    public void sendHostResponse() {
        final Intent intent = new Intent(Utils.activity, PinpadService.class);
        intent.setAction(PinpadService.ACTION_RESPONSE_FROM_HOST);
        intent.putExtra(PinpadService.EXTRA_MSG, currentMessage.getBytes());
        Utils.activity.startService(intent);

        this.setMesssageType(Constants.MessageType.NONE);
    }

    public void sendHostResponseWithExtraMsg() {
        final Intent intent = new Intent(Utils.activity, PinpadService.class);
        intent.setAction(PinpadService.ACTION_RESPONSE_FROM_HOST);
        intent.putExtra(PinpadService.EXTRA_MSG, currentMessage.getBytes());
        Utils.activity.startService(intent);
    }
}
