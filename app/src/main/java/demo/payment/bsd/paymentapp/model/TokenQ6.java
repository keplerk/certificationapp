package demo.payment.bsd.paymentapp.model;

public class TokenQ6 {
    private String diferimiento;
    private String numeroPagos;
    private String promocion;

    public String getDiferimiento() {
        return diferimiento;
    }

    public void setDiferimiento(String diferimiento) {
        this.diferimiento = diferimiento;
    }

    public String getNumeroPagos() {
        return numeroPagos;
    }

    public void setNumeroPagos(String numeroPagos) {
        this.numeroPagos = numeroPagos;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }
}
