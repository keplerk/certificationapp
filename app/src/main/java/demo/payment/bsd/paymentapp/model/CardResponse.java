package demo.payment.bsd.paymentapp.model;

public class CardResponse {
    private CardReadResponse cardReadResponse;
    private CardReadTags cardReadTags;
    private TokenQ6 tokenQ6;

    public TokenQ6 getTokenQ6() {
        return tokenQ6;
    }

    public void setTokenQ6(TokenQ6 tokenQ6) {
        this.tokenQ6 = tokenQ6;
    }

    public CardReadResponse getCardReadResponse() {
        return cardReadResponse;
    }

    public void setCardReadResponse(CardReadResponse cardReadResponse) {
        this.cardReadResponse = cardReadResponse;
    }

    public CardReadTags getCardReadTags() {
        return cardReadTags;
    }

    public void setCardReadTags(CardReadTags cardReadTags) {
        this.cardReadTags = cardReadTags;
    }
}
