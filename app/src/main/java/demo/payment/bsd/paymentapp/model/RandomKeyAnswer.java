package demo.payment.bsd.paymentapp.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class RandomKeyAnswer extends AnswerModel {
    private String transacitonId;
    private String serialNumber;
    private String encryptedBlockLength;
    private String encryptedBlock;
    private String randomKeyKcv;
    private String encryptedBlockCrc32;

    public String getTransacitonId() {
        return transacitonId;
    }

    public void setTransacitonId(String transacitonId) {
        this.transacitonId = transacitonId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getEncryptedBlockLength() {
        return encryptedBlockLength;
    }

    public void setEncryptedBlockLength(String encryptedBlockLength) {
        this.encryptedBlockLength = encryptedBlockLength;
    }

    public String getEncryptedBlock() {
        return encryptedBlock;
    }

    public void setEncryptedBlock(String encryptedBlock) {
        this.encryptedBlock = encryptedBlock;
    }

    public String getRandomKeyKcv() {
        return randomKeyKcv;
    }

    public void setRandomKeyKcv(String randomKeyKcv) {
        this.randomKeyKcv = randomKeyKcv;
    }

    public String getEncryptedBlockCrc32() {
        return encryptedBlockCrc32;
    }

    public void setEncryptedBlockCrc32(String encryptedBlockCrc32) {
        this.encryptedBlockCrc32 = encryptedBlockCrc32;
    }

    public static RandomKeyAnswer fromJson (String json) throws IOException {
        return new ObjectMapper().readValue(json, RandomKeyAnswer.class);
    }
}
