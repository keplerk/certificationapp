package demo.payment.bsd.paymentapp.model;

public class DukptInitializeResponse extends AnswerModel{
    public String transactionId = "";
    public String serialNumber;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
