package demo.payment.bsd.paymentapp.model;

import demo.payment.bsd.paymentapp.utils.Constants;

public class CardReadTags {
    public CardReadTags (String TAG_5F34, String TAG_4F, String TAG_5A, String TAG_5F20, String TAG_5F24, String TAG_5F25, String TAG_57, String TAG_5F2A, String TAG_82, String TAG_95, String TAG_9A, String TAG_9C, String TAG_9F02, String TAG_9F10, String TAG_9F1A, String TAG_9F17, String TAG_9F26, String TAG_9F27, String TAG_9F33, String TAG_9F40, String TAG_9F34, String TAG_9F36, String TAG_9F37, String TAG_9F30, String TAG_50, String TAG_9F0D, String TAG_9F0E, String TAG_9F0F, String TAG_5F28, String TAG_9F03, String TAG_9F09, String TAG_84, String TAG_9F35) {
        this.TAG_5F34 = TAG_5F34;
        this.TAG_4F = TAG_4F;
        this.TAG_5A = TAG_5A;
        this.TAG_5F20 = TAG_5F20;
        this.TAG_5F24 = TAG_5F24;
        this.TAG_5F25 = TAG_5F25;
        this.TAG_57 = TAG_57;
        this.TAG_5F2A = TAG_5F2A;
        this.TAG_82 = TAG_82;
        this.TAG_95 = TAG_95;
        this.TAG_9A = TAG_9A;
        this.TAG_9C = TAG_9C;
        this.TAG_9F02 = TAG_9F02;
        this.TAG_9F10 = TAG_9F10;
        this.TAG_9F1A = TAG_9F1A;
        this.TAG_9F17 = TAG_9F17;
        this.TAG_9F26 = TAG_9F26;
        this.TAG_9F27 = TAG_9F27;
        this.TAG_9F33 = TAG_9F33;
        this.TAG_9F40 = TAG_9F40;
        this.TAG_9F34 = TAG_9F34;
        this.TAG_9F36 = TAG_9F36;
        this.TAG_9F37 = TAG_9F37;
        this.TAG_9F30 = TAG_9F30;
        this.TAG_50 = TAG_50;
        this.TAG_9F0D = TAG_9F0D;
        this.TAG_9F0E = TAG_9F0E;
        this.TAG_9F0F = TAG_9F0F;
        this.TAG_5F28 = TAG_5F28;
        this.TAG_9F03 = TAG_9F03;
        this.TAG_9F09 = TAG_9F09;
        this.TAG_9F35 = TAG_9F35;
        this.TAG_84 = TAG_84;
    }

    public CardReadTags () { }

    public void setTag(String tag, String value){
        switch (tag.toUpperCase()){
            case Constants.Tags.TAG_5F34:
                this.TAG_5F34   = value;
                break;
            case Constants.Tags.TAG_4F:
                this.TAG_4F     = value;
                break;
            case Constants.Tags.TAG_5A:
                this.TAG_5A     = value;
                break;
            case Constants.Tags.TAG_5F20:
                this.TAG_5F20   = value;
                break;
            case Constants.Tags.TAG_5F24:
                this.TAG_5F24   = value;
                break;
            case Constants.Tags.TAG_5F25:
                this.TAG_5F25   = value;
                break;
            case Constants.Tags.TAG_57:
                this.TAG_57     = value;
                break;
            case Constants.Tags.TAG_5F2A:
                this.TAG_5F2A   = value;
                break;
            case Constants.Tags.TAG_82:
                this.TAG_82     = value;
                break;
            case Constants.Tags.TAG_95:
                this.TAG_95     = value;
                break;
            case Constants.Tags.TAG_9A:
                this.TAG_9A     = value;
                break;
            case Constants.Tags.TAG_9C:
                this.TAG_9C     = value;
                break;
            case Constants.Tags.TAG_9F02:
                this.TAG_9F02   = value;
                break;
            case Constants.Tags.TAG_9F10:
                this.TAG_9F10   = value;
                break;
            case Constants.Tags.TAG_9F1A:
                this.TAG_9F1A   = value;
                break;
            case Constants.Tags.TAG_9F17:
                this.TAG_9F17   = value;
                break;
            case Constants.Tags.TAG_9F26:
                this.TAG_9F26   = value;
                break;
            case Constants.Tags.TAG_9F27:
                this.TAG_9F27   = value;
                break;
            case Constants.Tags.TAG_9F33:
                this.TAG_9F33   = value;
                break;
            case Constants.Tags.TAG_9F40:
                this.TAG_9F40   = value;
                break;
            case Constants.Tags.TAG_9F34:
                this.TAG_9F34   = value;
                break;
            case Constants.Tags.TAG_9F36:
                this.TAG_9F36   = value;
                break;
            case Constants.Tags.TAG_9F37:
                this.TAG_9F37   = value;
                break;
            case Constants.Tags.TAG_9F30:
                this.TAG_9F30   = value;
                break;
            case Constants.Tags.TAG_50:
                this.TAG_50     = value;
                break;
            case Constants.Tags.TAG_9F0D:
                this.TAG_9F0D   = value;
                break;
            case Constants.Tags.TAG_9F0E:
                this.TAG_9F0E   = value;
                break;
            case Constants.Tags.TAG_9F0F:
                this.TAG_9F0F   = value;
                break;
            case Constants.Tags.TAG_5F28:
                this.TAG_5F28   = value;
                break;
            case Constants.Tags.TAG_9F03:
                this.TAG_9F03   = value;
                break;
            case Constants.Tags.TAG_9F09:
                this.TAG_9F09   = value;
                break;
            case Constants.Tags.TAG_84:
                this.TAG_84     = value;
                break;
            case Constants.Tags.TAG_9F35:
                this.TAG_9F35   = value;
                break;
        }

    }

    public String getTagValue(String tag){
        String result = "";
        switch (tag.toUpperCase()){
            case Constants.Tags.TAG_5F34:
                result = this.TAG_5F34;
                break;
            case Constants.Tags.TAG_4F:
                result = this.TAG_4F;
                break;
            case Constants.Tags.TAG_5A:
                result = this.TAG_5A;
                break;
            case Constants.Tags.TAG_5F20:
                result = this.TAG_5F20;
                break;
            case Constants.Tags.TAG_5F24:
                result = this.TAG_5F24;
                break;
            case Constants.Tags.TAG_5F25:
                result = this.TAG_5F25;
                break;
            case Constants.Tags.TAG_57:
                result = this.TAG_57;
                break;
            case Constants.Tags.TAG_5F2A:
                result = this.TAG_5F2A;
                break;
            case Constants.Tags.TAG_82:
                result = this.TAG_82;
                break;
            case Constants.Tags.TAG_95:
                result = this.TAG_95;
                break;
            case Constants.Tags.TAG_9A:
                result = this.TAG_9A;
                break;
            case Constants.Tags.TAG_9C:
                result = this.TAG_9C;
                break;
            case Constants.Tags.TAG_9F02:
                result = this.TAG_9F02;
                break;
            case Constants.Tags.TAG_9F10:
                result = this.TAG_9F10;
                break;
            case Constants.Tags.TAG_9F1A:
                result = this.TAG_9F1A;
                break;
            case Constants.Tags.TAG_9F17:
                result = this.TAG_9F17;
                break;
            case Constants.Tags.TAG_9F26:
                result = this.TAG_9F26;
                break;
            case Constants.Tags.TAG_9F27:
                result = this.TAG_9F27;
                break;
            case Constants.Tags.TAG_9F33:
                result = this.TAG_9F33;
                break;
            case Constants.Tags.TAG_9F40:
                result = this.TAG_9F40;
                break;
            case Constants.Tags.TAG_9F34:
                result = this.TAG_9F34;
                break;
            case Constants.Tags.TAG_9F36:
                result = this.TAG_9F36;
                break;
            case Constants.Tags.TAG_9F37:
                result = this.TAG_9F37;
                break;
            case Constants.Tags.TAG_9F30:
                result = this.TAG_9F30;
                break;
            case Constants.Tags.TAG_50:
                result = this.TAG_50;
                break;
            case Constants.Tags.TAG_9F0D:
                result = this.TAG_9F0D;
                break;
            case Constants.Tags.TAG_9F0E:
                result = this.TAG_9F0E;
                break;
            case Constants.Tags.TAG_9F0F:
                result = this.TAG_9F0F;
                break;
            case Constants.Tags.TAG_5F28:
                result = this.TAG_5F28;
                break;
            case Constants.Tags.TAG_9F03:
                result = this.TAG_9F03;
                break;
            case Constants.Tags.TAG_9F09:
                result = this.TAG_9F09;
                break;
            case Constants.Tags.TAG_84:
                result = this.TAG_84;
                break;
            case Constants.Tags.TAG_9F35:
                result = this.TAG_9F35;
                break;
        }

        return result;
    }

    String TAG_5F34 = "";
    String TAG_4F   = "";
    String TAG_5A   = "";
    String TAG_5F20 = "";
    String TAG_5F24 = "";
    String TAG_5F25 = "";
    String TAG_57   = "";
    String TAG_5F2A = "";
    String TAG_82   = "";
    String TAG_95   = "";
    String TAG_9A   = "";
    String TAG_9C   = "";
    String TAG_9F02 = "";
    String TAG_9F10 = "";
    String TAG_9F1A = "";
    String TAG_9F17 = "";
    String TAG_9F26 = "";
    String TAG_9F27 = "";
    String TAG_9F33 = "";
    String TAG_9F40 = "";
    String TAG_9F34 = "";
    String TAG_9F36 = "";
    String TAG_9F37 = "";
    String TAG_9F30 = "";
    String TAG_50   = "";
    String TAG_9F0D = "";
    String TAG_9F0E = "";
    String TAG_9F0F = "";
    String TAG_5F28 = "";
    String TAG_9F03 = "";
    String TAG_9F09 = "";
    String TAG_84   = "";
    String TAG_9F35 = "";
}
