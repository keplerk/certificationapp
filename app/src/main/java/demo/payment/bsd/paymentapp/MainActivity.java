package demo.payment.bsd.paymentapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import demo.payment.bsd.paymentapp.activities.ProcessActivity;
import demo.payment.bsd.paymentapp.activities.SaleActivity;
import demo.payment.bsd.paymentapp.utils.Constants;
import demo.payment.bsd.paymentapp.utils.Pinpad;
import demo.payment.bsd.paymentapp.utils.Utils;

public class MainActivity extends AppCompatActivity {

    public static MainActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setEvents();
        Utils.requestPermissions(this);
        Utils.activity = this;
        mActivity = this;
    }

    Button btnInit;
    Button btnSale;
    Button btnProcess;

    private void setEvents() {
        btnInit = (Button)findViewById(R.id.btnInit);
        btnSale = (Button)findViewById(R.id.btnSale);
        btnProcess = (Button)findViewById(R.id.btnProcess);

        btnInit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.StartProcessDialog("Conectando con pinpad");
                Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_RANDOM_KEY);
                Pinpad.getInstance().startService();
            }
        });

        btnSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SaleActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProcessActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }
}
