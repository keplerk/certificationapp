package demo.payment.bsd.paymentapp.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import demo.payment.bsd.paymentapp.R;
import demo.payment.bsd.paymentapp.utils.Messages;
import demo.payment.bsd.paymentapp.utils.Pinpad;
import demo.payment.bsd.paymentapp.utils.Utils;

public class SaleNormalActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saledetail);

        setEvents();
    }

    Button btnCheckOut;
    TextView txtPrice;

    private void setEvents () {
        btnCheckOut = (Button)findViewById(R.id.btnCheckout);
        txtPrice = (TextView)findViewById(R.id.txtPrice);

        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtPrice.getText().length() < 1) {
                    Utils.ShowToast("Debes ingresar una cantidad valida");
                } else {
                    HideSoftKeyboard(getCurrentFocus());
                    Utils.StartProcessDialog(SaleNormalActivity.this, "Conectando con pinpad");
                    Messages.buildSaleMessage(txtPrice.getText().toString());
                    Pinpad.getInstance().startService();
                }
            }
        });
    }

    private void HideSoftKeyboard (View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
