package demo.payment.bsd.paymentapp.utils;

import java.net.URI;

import es.itos.pagomovil.pinpad.Message;

public class Constants {
    public static final String RSA_TEMP = "DF71C129AC324DF0144D6A4A6383E81EF0E5510ECF658523764D84BE0E3C6B02EF94679C9680EAC1C4A28323CEB911A2431EDB5CE14DC07F47A7BF94875963188F27B4C20E7C2CE8F0D5113260970178CB3ACAEB4F2ACCB42F0203BDDAAB383F4C0552E480900CF9FF10C10D89F7EB4AA72A9AF97DB280E82C0137E2D4CC9C9CDC990C7A7D458815403600681A210162723976A596F91D3C8A5D5C946754C3114E74074F3ACCEFD951201C6CD454220E365AC8F5D6ACCE76BD997DE4B690EA9F5318C53FB19650D37F499721C13239F21EB5849A434D219C2D7D4DA27875FCB64638F8FDF12CA363CE5D5E4E7C4698CD6FC64F9C9BCB006BC5399AEA349E7091";
    public static final String RSA_SIGNATURE = "N000ARFT22";
    public static final String RSA_EXP_LENGTH = "6";
    public static final String RSA_EXP = "010001";

    public static final int PINPAD_REQUEST = Message.PARAMETERS_UPDATE;
    public static final int SALE_REQUEST = Message.BEGIN_PAYMENT_TRANSACTION;
    public static final int EMV_NOTIFY = Message.ACKNOWLEDGE_TO_TRANSACTION_CONFIRMATION;
    public static final int MAGSTRIP_NOTIFY = Message.ACKNOWLEDGE_TO_MAGSTRIPE_READING_NOTIFICATION;
    public static final int ANSWER_TO_ONLINE_EMV_REQUEST = Message.ANSWER_TO_ONLINE_EMV_REQUEST;
    public static final int ACKNOWLEDGE_TO_TRANSACTION_CONFIRMATION = Message.ACKNOWLEDGE_TO_TRANSACTION_CONFIRMATION;

    public static final String LOG_NAME = "BSD_PAYMENT_APP";

    public class TransactionCodes {
        public static final String DUKPT_KEY_GENERATE = "12";
        public static final String DUKPT_KEY_INSTALL = "13";
        public static final String SALE_REQUEST = "00";
        public static final String REFUND_REQUEST = "20";
        public static final String CONFIGURATION_REQUEST = "11";
    }

    public class Error {
        public static final String STATUS_ERROR_BLUETOOTH_NOT_ENABLED = "Error : Bluetooth no activado";
        public static final String STATUS_ERROR_COMMUNICATION_ERROR = "Error : No se pudo establecer comunicación con Pinpad";
        public static final String STATUS_ERROR_INVALID_OPERATION_DATA = "Error : Ocurrio de datos con el pinpad";
        public static final String STATUS_ERROR_PINPAD_NOT_READY = "Error : Pinpad no listo";
        public static final String STATUS_ERROR_TIMEOUT = "Error : Tiempo de espera agotado, no se pudo conectar con pinpad";
        public static final String STATUS_ERROR_OPERATION_CANCELED = "Operación cancelada";
        public static final String STATUS_ERROR_PINPAD_BUSY = "Error : Pinpad ocupado";
    }

    public enum MessageType {
        REQUEST_RANDOM_KEY,
        REQUEST_DUKPT_INSTALL,
        REQUEST_SALE,
        NOTIFY_SALE_MAGSTRIP,
        NOTIFY_SALE_EMV,
        REQUEST_SALE_MONTHS,
        REQUEST_REFOUND,
        EMV_110_NOTIFY,
        NONE,
        EMV_FINISH,
        EMV_ERROR
    }

    public enum AnswerType {
        ANSWER_RANDOM_KEY_REQUEST,
        REQUEST_DUKPT_INSTALLATION,
        ANSWER_DUKPT_INSTALLATION,
        REQUEST_CONFIGURATION,
        REQUEST_TRANSACTION_EMV,
        REQUEST_TRANSACTION_MAGSTRIPE,
        NONE,
        REQUEST_101
    }

    public class CodeRequest {
        public static final int DUKPT_INITIALIZE = 1;

        public static final int DUKPT_INICIALIZED = 10;
    }

    public class Messages {
        public static final String ACTION_OPERATION_END = "mx.bsd.process.intent.action.OPERATION.END";
        public static final String ACTION_OPERATION_REQUEST = "mx.bsd.process.intent.action.OPERATION.REQUEST";

        public static final String ACTION_OPERATION_START = "mx.bsd.process.intent.action.START";

        public static final int STATUS_READY = 0;
        public static final int STATUS_FINISH = 1;
        public static final int STATUS_UPDATE = 2;

        public static final int RESULT_CANCELED = 0;
        public static final int RESULT_OK = 1;

        public static final String EXTRA_DATA = "mx.bsd.process.intent.extra.DATA";
        public static final String EXTRA_STATUS = "mx.bsd.process.intent.extra.STATUS";
        public static final String EXTRA_MSG = "mx.bsd.process.intent.extra.MSG";
        public static final String EXTRA_URL = "mx.bsd.process.intent.extra.URL";
        public static final String EXTRA_CODE = "mx.bsd.process.intent.extra.CODE";
    }

    public class Tags {
        public static final String TAG_5F34 = "5F34";
        public static final String TAG_4F   = "4F";
        public static final String TAG_5A   = "5A";
        public static final String TAG_5F20 = "5F20";
        public static final String TAG_5F24 = "5F24";
        public static final String TAG_5F25 = "5F25";
        public static final String TAG_57   = "57";
        public static final String TAG_5F2A = "5F2A";
        public static final String TAG_82   = "82";
        public static final String TAG_95   = "95";
        public static final String TAG_9A   = "9A";
        public static final String TAG_9C   = "9C";
        public static final String TAG_9F02 = "9F02";
        public static final String TAG_9F10 = "9F10";
        public static final String TAG_9F1A = "9F1A";
        public static final String TAG_9F17 = "9F17";
        public static final String TAG_9F26 = "9F26";
        public static final String TAG_9F27 = "9F27";
        public static final String TAG_9F33 = "9F33";
        public static final String TAG_9F40 = "9F40";
        public static final String TAG_9F34 = "9F34";
        public static final String TAG_9F36 = "9F36";
        public static final String TAG_9F37 = "9F37";
        public static final String TAG_9F30 = "9F30";
        public static final String TAG_9F35 = "9F35";
        public static final String TAG_50   = "50";
        public static final String TAG_9F0D = "9F0D";
        public static final String TAG_9F0E = "9F0E";
        public static final String TAG_9F0F = "9F0F";
        public static final String TAG_5F28 = "5F28";
        public static final String TAG_9F03 = "9F03";
        public static final String TAG_9F09 = "9F09";
        public static final String TAG_84   = "84";
    }

    public static final String [] ARRAY_TAG = {
            Tags.TAG_5F34,
            Tags.TAG_4F,
            Tags.TAG_5A,
            Tags.TAG_5F20,
            Tags.TAG_5F24,
            Tags.TAG_5F25,
            Tags.TAG_57,
            Tags.TAG_5F2A,
            Tags.TAG_82,
            Tags.TAG_95,
            Tags.TAG_9A,
            Tags.TAG_9C,
            Tags.TAG_9F02,
            Tags.TAG_9F10,
            Tags.TAG_9F1A,
            Tags.TAG_9F17,
            Tags.TAG_9F26,
            Tags.TAG_9F27,
            Tags.TAG_9F33,
            Tags.TAG_9F40,
            Tags.TAG_9F34,
            Tags.TAG_9F36,
            Tags.TAG_9F37,
            Tags.TAG_9F30,
            Tags.TAG_50,
            Tags.TAG_9F0D,
            Tags.TAG_9F0E,
            Tags.TAG_9F0F,
            Tags.TAG_5F28,
            Tags.TAG_9F03,
            Tags.TAG_9F35,
            Tags.TAG_9F09,
            Tags.TAG_84
    };

    //public static final String BASE_REST_URL = "http://192.168.1.65:8080/";
    public static final String BASE_REST_URL = "http://devbroker.bsdap.com:8080/SwitchProsa/";
    public static final String BASE_STOMP_URL = "ws://52.8.215.37:15674/ws";
}
