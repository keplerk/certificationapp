package demo.payment.bsd.paymentapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Date;

import demo.payment.bsd.paymentapp.model.CardReadResponse;
import demo.payment.bsd.paymentapp.model.CardReadTags;
import demo.payment.bsd.paymentapp.model.CardResponse;
import demo.payment.bsd.paymentapp.model.DukptInitializeResponse;
import demo.payment.bsd.paymentapp.model.DukptResponse;
import demo.payment.bsd.paymentapp.model.RandomKeyAnswer;
import demo.payment.bsd.paymentapp.model.ReverseModel;
import demo.payment.bsd.paymentapp.model.SaleResponse;
import demo.payment.bsd.paymentapp.processors.MessageProcessor;
import demo.payment.bsd.paymentapp.tasks.ProcessingTask;
import demo.payment.bsd.paymentapp.tasks.SaleTask;
import demo.payment.bsd.paymentapp.tasks.InitializeTask;
import demo.payment.bsd.paymentapp.tasks.actions.IStompEvents;
import demo.payment.bsd.paymentapp.utils.Constants;
import demo.payment.bsd.paymentapp.utils.Messages;
import demo.payment.bsd.paymentapp.utils.Pinpad;
import demo.payment.bsd.paymentapp.utils.Utils;
import es.itos.pagomovil.service.PinpadService;

public class PinpadReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            final String action = intent.getAction();
            int status = intent.getIntExtra(PinpadService.EXTRA_STATUS, -1);
            final String data = intent.getStringExtra("es.itos.pinpad.intent.extra.DATA");
            final byte[] byteMsg = intent.getByteArrayExtra("es.itos.pinpad.intent.extra.MSG");
            final String message = byteMsg == null ? "" : new String(byteMsg);

            switch (action) {
                case PinpadService.ACTION_OPERATION_END:
                    switch (status) {
                        case PinpadService.STATUS_ERROR_BLUETOOTH_NOT_ENABLED:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_BLUETOOTH_NOT_ENABLED);
                            break;
                        case PinpadService.STATUS_ERROR_COMMUNICATION_ERROR:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_COMMUNICATION_ERROR);
                            break;
                        case PinpadService.STATUS_ERROR_INVALID_OPERATION_DATA:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_INVALID_OPERATION_DATA);
                            break;
                        case PinpadService.STATUS_ERROR:
                            if (Utils.reverseModel != null) {
                                CardReadResponse responseCard = MessageProcessor.processMessage(Constants.AnswerType.REQUEST_101, data);
                                Utils.reverseModel.setDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
                                new ProcessingTask(new IStompEvents() {
                                    @Override
                                    public void onComplete() {
                                        Utils.logI("Stop conectado");
                                    }

                                    @Override
                                    public void onError(Throwable ex) { }

                                    @Override
                                    public void onNext(String stompMessage) {
                                        try {
                                            SaleResponse response = new ObjectMapper().readValue(stompMessage, SaleResponse.class);
                                            Utils.StopProcessDialog();
                                            Utils.ShowToast("Transacción Rechazada");
                                        } catch (Exception e) {
                                            Utils.logE(e);
                                        }
                                    }
                                }).execute(responseCard);
                            } else {
                                Utils.StopProcessDialog();
                                final Intent intentError = new Intent(PinpadService.ACTION_CANCEL_OPERATION_REQUEST);
                                context.startService(intentError);
                            }
                            break;
                        case PinpadService.STATUS_ERROR_PINPAD_NOT_READY:
                            Utils.StopProcessDialog();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_PINPAD_NOT_READY);
                            break;
                        case PinpadService.STATUS_OPERATION_CANCELLED:
                            Utils.StopProcessDialog();
                            Pinpad.getInstance().stopService();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_OPERATION_CANCELED);
                            break;
                        case PinpadService.STATUS_ERROR_TIME_OUT:
                            Utils.StopProcessDialog();
                            Pinpad.getInstance().stopService();
                            Utils.ShowToast(Constants.Error.STATUS_ERROR_TIMEOUT);
                            break;


                        case PinpadService.STATUS_READY:
                            switch (Pinpad.getInstance().getMesssageType()) {
                                case REQUEST_RANDOM_KEY:
                                    Utils.UpdateProcessDialogMessage("Solicitando llave de inicialización");
                                    Messages.buildDukptRequest();
                                    Pinpad.getInstance().sendRequest();
                                    break;
                                case REQUEST_DUKPT_INSTALL:
                                    Pinpad.getInstance().sendRequest();
                                    break;
                                case REQUEST_SALE:
                                    Utils.UpdateProcessDialogMessage("Solicitando cobro");
                                    Pinpad.getInstance().sendRequest();
                                    break;
                                case REQUEST_REFOUND:
                                    Utils.UpdateProcessDialogMessage("Solicitando información");
                                    Pinpad.getInstance().sendRequest();
                                    break;
                            }
                            break;


                        case PinpadService.STATUS_FINISH:
                            Pinpad.getInstance().stopService();
                            Constants.AnswerType answer = MessageProcessor.getMessageType(data);
                            switch (answer) {
                                case ANSWER_RANDOM_KEY_REQUEST:
                                    Utils.UpdateProcessDialogMessage("Solicitando llave DUKPT");
                                    RandomKeyAnswer randomKeyAnswer = MessageProcessor.processMessage(answer, data);

                                    new InitializeTask(new IStompEvents() {
                                        @Override
                                        public void onComplete() {
                                            Utils.logI("Stop conectado");
                                        }

                                        @Override
                                        public void onError(Throwable ex) { }

                                        @Override
                                        public void onNext(String stompMessage) {
                                            try {
                                                Utils.UpdateProcessDialogMessage("Installando llave DUKPT");
                                                if (stompMessage.contains("false")) {
                                                    SaleResponse response = new ObjectMapper().readValue(stompMessage, SaleResponse.class);
                                                    Utils.StopProcessDialog();
                                                    Utils.ShowToast(response.getMessage());
                                                    return;
                                                }
                                                DukptResponse dukpt = new ObjectMapper().readValue(stompMessage, DukptResponse.class);
                                                Messages.buildDukptInstallation(dukpt);
                                                Pinpad.getInstance().startService();
                                            } catch (Exception e) {
                                                Utils.logE(e);
                                            }
                                        }
                                    }).execute(randomKeyAnswer);
                                    break;
                                case ANSWER_DUKPT_INSTALLATION:
                                    DukptInitializeResponse response = MessageProcessor.processMessage(answer, data);
                                    Utils.StopProcessDialog();

                                    if (!response.getStatus().equals("0000")) {
                                        Utils.ShowToast("Error al inicializar pinpad");
                                    } else {
                                        Utils.ShowToast("Pinpad Inicializado");
                                    }
                                    break;
                                case REQUEST_CONFIGURATION:
                                    Pinpad.getInstance().stopService();
                                    CardReadResponse configuration = MessageProcessor.processMessage(answer, data);
                                    Utils.UpdateProcessDialogMessage("Procesando movimiento");

                                    new ProcessingTask(new IStompEvents() {
                                        @Override
                                        public void onComplete() {
                                            Utils.logI("Stop conectado");
                                        }

                                        @Override
                                        public void onError(Throwable ex) { }

                                        @Override
                                        public void onNext(String stompMessage) {
                                            try {
                                                SaleResponse response = new ObjectMapper().readValue(stompMessage, SaleResponse.class);
                                                Utils.StopProcessDialog();
                                                Utils.ShowToast(response.getMessage());
                                            } catch (Exception e) {
                                                Utils.logE(e);
                                            }
                                        }
                                    }).execute(configuration);
                                    break;
                                case NONE:
                                    Utils.StopProcessDialog();
                                    switch (Pinpad.getInstance().getMesssageType()) {
                                        case EMV_FINISH:
                                            Utils.ShowToast("Transacción Aprobada");
                                            break;
                                    }
                                    break;
                            }
                            break;

                    }
                    break;

                case PinpadService.ACTION_REQUEST_FOR_HOST:
                    Constants.AnswerType answer = MessageProcessor.getMessageType(message);

                    switch (answer) {
                        case REQUEST_101:
                            CardReadResponse responseCard = MessageProcessor.processMessage(answer, message);
                            Pinpad.getInstance().setMesssageType(Constants.MessageType.EMV_FINISH);
                            Messages.buildEmvAnswer111();
                            Pinpad.getInstance().sendHostResponseWithExtraMsg();
                            break;
                        case REQUEST_TRANSACTION_EMV:
                            CardResponse cardEmv = MessageProcessor.processMessage(answer, message);
                            if (Utils.tokenQ6 != null) {
                                cardEmv.setTokenQ6(Utils.tokenQ6);
                            }
                            Utils.UpdateProcessDialogMessage("Procesando pago");
                            new SaleTask(new IStompEvents() {
                                @Override
                                public void onComplete() {
                                    Utils.logI("Stop conectado");
                                }

                                @Override
                                public void onError(Throwable ex) { }

                                @Override
                                public void onNext(String stompMessage) {
                                    try {
                                        SaleResponse response = new ObjectMapper().readValue(stompMessage, SaleResponse.class);

                                        if (response.getResponseCode()) {
                                            Utils.reverseModel = response.getMessageObject();
                                        }

                                        Utils.UpdateProcessDialogMessage("Procesando respuesta");
                                        Messages.buildEmvAnswer110(response.getResponseCode());
                                        Pinpad.getInstance().sendHostResponse();
                                    } catch (Exception e) {
                                        Utils.logE(e);
                                    }
                                }
                                
                            }).execute(cardEmv);
                            break;
                        case REQUEST_TRANSACTION_MAGSTRIPE:
                            CardResponse cardMagstripe = MessageProcessor.processMessage(answer, message);
                            if (Utils.tokenQ6 != null) {
                                cardMagstripe.setTokenQ6(Utils.tokenQ6);
                            }

                            Utils.UpdateProcessDialogMessage("Procesando pago");

                            new SaleTask(new IStompEvents() {
                                @Override
                                public void onComplete() {
                                    Utils.logI("Stop conectado");
                                }

                                @Override
                                public void onError(Throwable ex) { }

                                @Override
                                public void onNext(String stompMessage) {
                                    try {
                                        SaleResponse response = new ObjectMapper().readValue(stompMessage, SaleResponse.class);
                                        Pinpad.getInstance().setMesssageType(Constants.MessageType.EMV_FINISH);
                                        Utils.UpdateProcessDialogMessage("Procesando respuesta");
                                        Messages.buildEmvAnswer0020();
                                        Pinpad.getInstance().sendHostResponse();
                                    } catch (Exception e) {
                                        Utils.logE(e);
                                    }
                                }
                            }).execute(cardMagstripe);
                            break;
                        default :
                            Pinpad.getInstance().stopService();
                            break;
                    }
                    Utils.tokenQ6 = null;
                    break;
            }
        } catch (Exception ex) {
            Pinpad.getInstance().stopService();
            Utils.StopProcessDialog();
            Utils.logE(ex);
        }
    }
}
