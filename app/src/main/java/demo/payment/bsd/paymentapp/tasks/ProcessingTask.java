package demo.payment.bsd.paymentapp.tasks;

import android.os.AsyncTask;

import org.json.JSONObject;

import demo.payment.bsd.paymentapp.model.CardReadResponse;
import demo.payment.bsd.paymentapp.model.ReverseModel;
import demo.payment.bsd.paymentapp.tasks.actions.IStompEvents;
import demo.payment.bsd.paymentapp.tasks.actions.RestRequester;
import demo.payment.bsd.paymentapp.tasks.actions.StompConnector;
import demo.payment.bsd.paymentapp.utils.Utils;

public class ProcessingTask extends AsyncTask<Object, Void, String> {
    private String result;

    private IStompEvents delegate = null;
    public ProcessingTask(IStompEvents delegate) {
        this.delegate = delegate;
    }

    public ProcessingTask() {}

    @Override
    protected String doInBackground(Object... objects) {
        try {
            CardReadResponse answer = (CardReadResponse) objects[0];
            final ReverseModel reverseModel = Utils.reverseModel;

            final JSONObject reverse = new JSONObject();
            reverse.put("date", reverseModel.getDate());
            reverse.put("traceNumber", reverseModel.getTicketNumber());
            reverse.put("pinpadSerial", answer.getP2());
            reverse.put("authorizationCode", reverseModel.getAuthorizationNumber());

            new StompConnector(new IStompEvents() {
                @Override
                public void onComplete() {
                    String uuid = Utils.GetUuid(false);
                    switch (reverseModel.getProcessType()) {
                        case 1:
                            RestRequester.SendRequest("audit/reversal/" + uuid + ".json", reverse);
                            break;
                        case 2:
                            RestRequester.SendRequest("audit/cancel/" + uuid + ".json", reverse);
                            break;
                        case 3:
                            RestRequester.SendRequest("audit/devolution/" + uuid + ".json", reverse);
                            break;
                    }
                }

                @Override
                public void onError(Throwable ex) {
                    delegate.onError(ex);
                }

                @Override
                public void onNext(String stompMessage) {
                    delegate.onNext(stompMessage);
                }
            }).runStomp(Utils.GetUuid(true));
        } catch (Exception ex) {
            Utils.logE(ex);
        }
        return null;
    }
}