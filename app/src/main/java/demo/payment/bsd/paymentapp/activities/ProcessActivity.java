package demo.payment.bsd.paymentapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import demo.payment.bsd.paymentapp.R;

public class ProcessActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process);


        setEvents();
    }

    Button btnReverse;
    Button btnCancel;
    Button btnRefound;

    private void setEvents (){
        btnReverse = (Button)findViewById(R.id.btnReverse);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnRefound = (Button)findViewById(R.id.btnRefound);

        btnReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startProcess(1);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startProcess(2);
            }
        });

        btnRefound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startProcess(3);
            }
        });
    }

    private void startProcess (int comeFrom) {
        Intent intent = new Intent(ProcessActivity.this, ProcessActionActivity.class);
        intent.putExtra("comefrom", comeFrom);
        ProcessActivity.this.startActivity(intent);
    }
}
