package demo.payment.bsd.paymentapp.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import demo.payment.bsd.paymentapp.R;
import demo.payment.bsd.paymentapp.utils.Messages;
import demo.payment.bsd.paymentapp.utils.Pinpad;
import demo.payment.bsd.paymentapp.utils.Utils;

public class SaleMonthsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salemonths);

        setEvents();
    }

    private Button btnCheckOut;
    private Spinner spinnerPromo;
    private TextView txtPrice;
    private TextView txtDiferimiento;
    private TextView txtMonths;

    private String [] promotions = {"03", "05", "07"};

    private void setEvents () {
        btnCheckOut = (Button)findViewById(R.id.btnCheckout);
        spinnerPromo = (Spinner)findViewById(R.id.spinnerPromo);
        txtPrice = (TextView)findViewById(R.id.txtPrice);
        txtDiferimiento = (TextView)findViewById(R.id.txtDiferimiento);
        txtMonths = (TextView)findViewById(R.id.txtMonths);

        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int promoPosition = spinnerPromo.getSelectedItemPosition();
                String promotion = promotions[promoPosition];

                if (txtPrice.getText().length() < 1 || txtPrice.getText().length() == 1 && txtPrice.getText().toString().equals("0")) {
                    Utils.ShowToast("Debes ingresar una cantidad valida");
                }
                else if (txtDiferimiento.getText().length() == 1 && txtDiferimiento.getText().toString().equals("0")) {
                    Utils.ShowToast("Diferimiento invalido");
                }
                else if (txtMonths.getText().length() < 1 || txtMonths.getText().length() == 1 && txtMonths.getText().toString().equals("0")) {
                    Utils.ShowToast("Número de meses invalidos");
                }
                else {
                    HideSoftKeyboard(getCurrentFocus());
                    Utils.StartProcessDialog(SaleMonthsActivity.this, "Conectando con pinpad");
                    Messages.buildSaleMonthMessage(txtPrice.getText().toString(), promotion, txtDiferimiento.getText().toString(), txtMonths.getText().toString());
                    Pinpad.getInstance().startService();
                }

            }
        });
    }

    private void HideSoftKeyboard (View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
