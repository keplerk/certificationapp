package demo.payment.bsd.paymentapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import demo.payment.bsd.paymentapp.R;
import demo.payment.bsd.paymentapp.model.ReverseModel;
import demo.payment.bsd.paymentapp.utils.Constants;
import demo.payment.bsd.paymentapp.utils.Messages;
import demo.payment.bsd.paymentapp.utils.Pinpad;
import demo.payment.bsd.paymentapp.utils.Utils;

public class ProcessActionActivity extends AppCompatActivity {

    int comefrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processaction);
        comefrom = getIntent().getIntExtra("comefrom", 0);

        setEvents();
    }

    Button btnProcessAction;
    TextView txtTicketNumber;
    TextView txtAprobNumber;
    DatePicker datePicker;

    private void setEvents () {
        btnProcessAction = (Button)findViewById(R.id.btnProcessAction);
        txtTicketNumber = (TextView)findViewById(R.id.txtTicketNumber);
        txtAprobNumber = (TextView)findViewById(R.id.txtAprobNumber);
        datePicker = (DatePicker)findViewById(R.id.saleDate);


        btnProcessAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtTicketNumber.getText().toString().equals("")) {
                    Utils.ShowToast("Debe ingresar un número de ticket");
                }
                else if (txtAprobNumber.getText().toString().equals("")) {
                    Utils.ShowToast("Debe ingresar un número de aprobación");
                } else {
                    ReverseModel reverseModel = new ReverseModel();
                    reverseModel.setAuthorizationNumber(txtAprobNumber.getText().toString());
                    reverseModel.setTicketNumber(txtTicketNumber.getText().toString());
                    reverseModel.setProcessType(comefrom);

                    String year = String.valueOf(datePicker.getYear());
                    String month = String.format("%02d", (datePicker.getMonth() + 1));
                    String day = String.format("%02d", datePicker.getDayOfMonth());
                    reverseModel.setDate(year + month + day);

                    Utils.reverseModel = reverseModel;

                    Utils.StartProcessDialog(ProcessActionActivity.this, "Conectando con pinpad");
                    Messages.buildConfig();
                    Pinpad.getInstance().startService();
                }
            }
        });
    }
}
