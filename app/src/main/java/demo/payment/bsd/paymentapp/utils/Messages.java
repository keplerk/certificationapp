package demo.payment.bsd.paymentapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import demo.payment.bsd.paymentapp.model.DukptResponse;
import demo.payment.bsd.paymentapp.model.SaleResponse;
import demo.payment.bsd.paymentapp.model.TokenQ6;

public class Messages {
    public static void buildDukptRequest() {
        String messageIdentifier    = Integer.valueOf(Constants.PINPAD_REQUEST).toString();
        String messageLength        = "";
        String transactionId        = Constants.TransactionCodes.DUKPT_KEY_GENERATE;
        String rsaPkLen             = String.format("%04d", Constants.RSA_TEMP.length());
        String rsaPk                = Constants.RSA_TEMP;
        String rsaExpLen            = Constants.RSA_EXP_LENGTH;
        String rsaExp               = Constants.RSA_EXP;

        Integer msgLength = messageIdentifier.length()
                + 4
                + transactionId.length()
                + rsaPkLen.length()
                + rsaPk.length()
                + rsaExpLen.length()
                + rsaExp.length();

        messageLength = String.format("%04d", msgLength);

        String msg = messageIdentifier
                + "0010"
                + transactionId;
//                + rsaPkLen
//                + rsaPk
//                + rsaExpLen
//                + rsaExp;

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildDukptInstallation(DukptResponse dukpt) {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_DUKPT_INSTALL);

        String messageIdentifier    = Integer.valueOf(Constants.PINPAD_REQUEST).toString();
        String messageLength        = String.format("%04d", 62 + dukpt.getCheckValue().length());
        String transactionId        = Constants.TransactionCodes.DUKPT_KEY_INSTALL;
        String encriptedDukptKey    = dukpt.getBdk();
        String ksn                  = dukpt.getKsn();
        String dukptKcv             = dukpt.getCheckValue();

        String msg  = messageIdentifier
                    + messageLength
                    + transactionId
                    + encriptedDukptKey
                    + ksn
                    + dukptKcv;

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildSaleMessage(String price) {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_SALE);
        String amount = String.format("%010d", Integer.parseInt(price.replaceAll("\\.|\\$", "")));
        Utils.transactionAmount = amount;

        String messageIdentifier        = Integer.valueOf(Constants.SALE_REQUEST).toString();
        String messageLength            = "0021";
        String transactionId            = Constants.TransactionCodes.SALE_REQUEST;
        String transactionAmount        = amount;
        String attempsBeforeFallback    = "1";

        String msg  = messageIdentifier
                    + messageLength
                    + transactionId
                    + transactionAmount
                    + attempsBeforeFallback;

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildSaleMonthMessage(String price, final String promotion, final String diferimiento, final String months) {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_SALE);
        String amount = String.format("%010d", Integer.parseInt(price.replaceAll("\\.|\\$", "")));
        Utils.transactionAmount = amount;

        String messageIdentifier        = Integer.valueOf(Constants.SALE_REQUEST).toString();
        String messageLength            = "0021";
        String transactionId            = Constants.TransactionCodes.SALE_REQUEST;
        String transactionAmount        = amount;
        String attempsBeforeFallback    = "1";

        String msg  = messageIdentifier
                + messageLength
                + transactionId
                + transactionAmount
                + attempsBeforeFallback;

        Utils.tokenQ6 = new TokenQ6() {{
            setDiferimiento(diferimiento);
            setNumeroPagos(months);
            setPromocion(promotion);
        }};

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildConfig() {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.REQUEST_REFOUND);

        String messageIdentifier        = Integer.valueOf(Constants.PINPAD_REQUEST).toString();
        String messageLength            = "0032";
        String transactionId            = Constants.TransactionCodes.CONFIGURATION_REQUEST;
        String commTimeout              = "0030";
        String userTimeout              = "0030";
        String systemDateAndHour        = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

        String msg  = messageIdentifier
                    + messageLength
                    + transactionId
                    + commTimeout
                    + userTimeout;

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildEmvAnswer110(Boolean isSuccess) {
        Pinpad.getInstance().setMesssageType(Constants.MessageType.EMV_110_NOTIFY);

        String messageIdentifier        = String.format("%04d", Integer.valueOf(Constants.ANSWER_TO_ONLINE_EMV_REQUEST));
        String messageLength            = "0024";
        String status                   = isSuccess ? "0001" : "0000";
        String tagsLength               = "0008";
        String tags                     = isSuccess ? "8A023030" : "8A023035";

        String msg  = messageIdentifier
                    + messageLength
                    + status
                    + tagsLength
                    + tags;

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildEmvAnswer111() {

        String messageIdentifier        = String.format("%04d", Integer.valueOf(Constants.ACKNOWLEDGE_TO_TRANSACTION_CONFIRMATION));
        String messageLength            = "0012";
        String status                   = "0001";

        String msg  = messageIdentifier
                    + messageLength
                    + status;

        Pinpad.getInstance().setCurrentMessage(msg);
    }

    public static void buildEmvAnswer0020 () {
        String messageIdentifier    = "0020";
        String messageLength        = "0012";
        String status               = "0001";

        String msg  = messageIdentifier
                    + messageLength
                    + status;

        Pinpad.getInstance().setCurrentMessage(msg);
    }
}
