package demo.payment.bsd.paymentapp.model;

public class SaleResponse {
    private boolean responseCode;
    private String message;
    private ReverseModel messageObject;


    public boolean isResponseCode() {
        return responseCode;
    }

    public ReverseModel getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(ReverseModel messageObject) {
        this.messageObject = messageObject;
    }

    public boolean getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(boolean responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
