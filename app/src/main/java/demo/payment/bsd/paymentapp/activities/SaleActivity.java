package demo.payment.bsd.paymentapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import demo.payment.bsd.paymentapp.R;

public class SaleActivity extends AppCompatActivity {

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);

        setEvents();
    }

    Button btnNormalSale;
    Button btnMonthSale;

    private void setEvents () {
        btnNormalSale = (Button)findViewById(R.id.btnSale);
        btnMonthSale = (Button)findViewById(R.id.btnMonthSale);

        btnNormalSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SaleActivity.this, SaleNormalActivity.class);
                SaleActivity.this.startActivity(intent);
            }
        });

        btnMonthSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SaleActivity.this, SaleMonthsActivity.class);
                SaleActivity.this.startActivity(intent);
            }
        });
    }
}
